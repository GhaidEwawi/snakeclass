# include "Snake.h"


int Snake::getSize(){
	return size;
};


Node* Snake::getHead(){
	return head;
}


int Node::node_instances = 0;

bool Snake::isPartOfSnake(Point y) const {
	Node* t;
	t = head->next;
	while( t->next != nullptr ) {
		if( t->pos == y )
			return true;
		t = t->next;
	}
	return false;
};

void Snake::moveBody() const {

	Node* t= new Node;
	t->pos = head->pos;
	t->next = head->next;
	head->next = t;

	t = head;

	while( t->next->next != nullptr )
		t = t->next;

	delete(t->next);
	t->next = nullptr;

};

void Snake::ateFood(){

	Node* t = new Node;
	t->pos = head->pos;
	t->next = nullptr;

	Node* x = head;
	while ( x->next != nullptr)
		x = x->next;

	x->next = t;
	size++;


};

void Snake::move(directions direction){
	moveBody();
	switch(direction){
		case UP:
			head->pos.y -= 1;
			break;

		case RIGHT:
			head->pos.x += 1;
			break;

		case DOWN:
			head->pos.y += 1;
			break;

		case LEFT:
			head->pos.x -= 1;
			break;
		}
};

void Snake::destroySnake( Node* nodePtr ){
	if ( nodePtr == nullptr ) {
		this->head = nullptr;
		return;
	}
	destroySnake( nodePtr->next );
	delete(nodePtr);
}

Snake::Snake(){
	size = 3;
	Node *x = new Node;

	x->pos = Point(12, 12);
	x->next = new Node;
	x->next->pos = Point(11, 12);
	x->next->next = new Node;
	x->next->next->pos = Point(10, 12);
	x->next->next->next = nullptr;
	head = x;
};