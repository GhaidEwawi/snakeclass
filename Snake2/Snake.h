#pragma once

struct Point {
	int x;
	int y;

	bool operator <(const Point& pt) const{
    return (x < pt.x) || ((!(pt.x < x)) && (y < pt.y));
	}

	bool operator ==(const Point& pt) const{
		return (x == pt.x) && (y == pt.y);
	}
	Point(const Point &p2){
		x = p2.x; y = p2.y;
	} 

	Point(int one = 0, int two = 0){
		x = one;
		y = two;
	}

};

enum directions { UP, RIGHT, DOWN, LEFT };

struct Node {

	static int node_instances;
	Point pos;
	Node* next;
	
	Node(){
		node_instances++;
	}
	~Node(){
		node_instances--;
	}
};


class Snake {

private:
	Node* head;
	int size;
	void moveBody() const;
	

public:
	
	Snake();
	void ateFood();
	void move(directions direction);
	int getSize();
	Node* getHead();

	//bool intersect(Point pt) const;
	bool isPartOfSnake(Point y) const;
	void destroySnake( Node* head );
	
};

