# include "SnakeGame.h"


SnakeGame::SnakeGame(){
	windowWidth = 0;
	windowHeight = 0;
}


// Use this to fill a rectangle of the hDC with a specific color.
void FillRect(HDC hDC, const RECT* pRect, COLORREF color)
{
	COLORREF oldColor = SetBkColor(hDC, color);
	ExtTextOut(hDC, 0, 0, ETO_OPAQUE, pRect, TEXT(""), 0, 0);
	SetBkColor(hDC, oldColor);
}


int SnakeGame::randomGenerator() const {
	return rand() % (600/scale);
	//return rand() % 23 * 25;
	/*int r;
	while(true){
		r = (rand() % 575);
		if(r % 25 == 0)
			return r;
	}*/
}

void SnakeGame::drawObstacles(HDC hDC) const {

	RECT line = {0 , 0 , 600, 5};
	FillRect(hDC, &line, RGB(0, 0, 255));
	RECT line2 = {0, 0, 5, 600};
	FillRect(hDC, &line2, RGB(0, 0, 255));
	RECT line3 = {590, 0, 595, 595};
	FillRect(hDC, &line3, RGB(0, 0, 255));
	RECT line4 = {0, 590, 595, 595};
	FillRect(hDC, &line4, RGB(0, 0, 255));

}

void SnakeGame::endGame(HWND hWnd){
	active = false;
	MessageBox(hWnd, TEXT("YOU LOST!!!"), TEXT("Snake"), MB_OK );
	mySnake.destroySnake(mySnake.getHead());
	Initialize(hWnd);
}

void SnakeGame::drawSnake(HDC hDC, Node* snake){
	Node* temp = snake;
	while ( temp != NULL ) {
		if( temp == snake )
			FillRect(hDC , &getRectangle(hDC, temp->pos), RGB(31, 100, 255));
		else
			FillRect(hDC , &getRectangle(hDC, temp->pos), RGB(31, 192, 242));
		temp = temp->next;
	}
}

RECT SnakeGame::getRectangle(HDC hDC, Point leftTop) const {
	RECT getRectangle;
	getRectangle.left = leftTop.x * scale;
	getRectangle.top = leftTop.y * scale;
	getRectangle.bottom = getRectangle.top + scale - 5;
	getRectangle.right = getRectangle.left + scale - 5;
	return getRectangle;
}

void SnakeGame::Initialize(HWND hWnd){
	RECT rClient;
		GetClientRect(hWnd, &rClient);
		windowWidth = rClient.right - rClient.left; // rClient.top and rClient.left are always 0.
		windowHeight = rClient.bottom - rClient.top;

		OutputDebugStringA("My game has been initialized. This text should be shown in the 'output' window in VS");
		
		direction = RIGHT;
		
		//	start the game at position 300 300
		//	initialze the snake with length of 3.

		scale = 25;
		foodPosition.x = 5;
		foodPosition.y = 5;
		//	food start position

		active = true;
		mySnake.destroySnake(mySnake.getHead());
		mySnake = Snake();
}


void SnakeGame::OnTimer(HWND hWnd){
	if( !active )
			return;

	
		HDC hDC = GetDC(hWnd);
		RECT rClient;
		GetClientRect(hWnd, &rClient);
		FillRect(hDC, &rClient, RGB(0, 0, 0)); // Clear the window to blackness.
		char text[256] = { 0 };
		sprintf_s(text, "Length %d, nodes: %d", mySnake.getSize(), Node::node_instances);
		RECT rText = { 0, 0, rClient.right, 40 };
		DrawTextLine(hWnd, hDC, text, &rText, RGB(255, 120, 120));
		

		// Handling input Queue of snake movement.
		if( !input.empty() ) {
			if(direction == (input.front() + 2) % 4 )
				input.pop();
			else {
				direction = input.front();
				input.pop();
			}
		} 
		
		mySnake.move(direction);

		drawSnake(hDC, mySnake.getHead());

		// Head color RGB(31, 100, 255)
		// Body color RGB(31, 192, 242)

		if( mySnake.isPartOfSnake(mySnake.getHead()->pos))
			endGame(hWnd);

		RECT food = getRectangle(hDC, foodPosition);
		FillRect(hDC , &food, RGB(0, 255, 0));

		if(mySnake.getHead()->pos.x == foodPosition.x 
			&& mySnake.getHead()->pos.y == foodPosition.y){

			mySnake.ateFood();
			do {
				foodPosition.x = randomGenerator();
				foodPosition.y = randomGenerator();
			} while ( mySnake.isPartOfSnake(foodPosition) );

		}



		if( mySnake.getSize() == 529 ) {
			active = false;
			MessageBox(hWnd, TEXT("CONGRATULATIONS, YOU WON!!! "), TEXT("Snake"), MB_OK );
			mySnake.destroySnake(mySnake.getHead());
			Initialize(hWnd);
		}
		if(mySnake.getHead()->pos.x >= 600 / scale 
			|| mySnake.getHead()->pos.y >= 600 / scale 
			|| mySnake.getHead()->pos.x < 0 
			|| mySnake.getHead()->pos.y < 0)

			endGame(hWnd);	//DestroyWindow(hWnd);
			
		drawObstacles(hDC);
}


bool SnakeGame::OnKeyDown(WPARAM keyCode){
	if( keyCode == VK_SPACE )
			active = !active;


		// This part of the code prevents the snake from going 
		// in the exact opposite of current direction
		if(!input.empty()){
			if(keyCode == VK_DOWN  && input.back() == 0)
				return 1;
			else if(keyCode == VK_UP  && input.back() == 2)
				return 1;
			else if(keyCode == VK_RIGHT  && input.back() == 3)
				return 1;
			else if(keyCode == VK_LEFT  && input.back() == 1)
				return 1;
		}

		//This part handles direction when the direction change is Valid
		if (keyCode == VK_UP)
			input.push(UP);
		if(keyCode == VK_DOWN )
			input.push(DOWN);
		if( keyCode == VK_LEFT)
			input.push(LEFT);
		if(keyCode == VK_RIGHT)
			input.push(RIGHT);

		return true;
		
		return false; // They key pressed does not interest us. Let the OS handle it.
}
