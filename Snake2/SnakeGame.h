# pragma once

# include <Windows.h>
# include "Snake.h"
# include <cstdlib>
# include <queue>

// Use this to fill a rectangle of the hDC with a specific color.
void FillRect(HDC hDC, const RECT* pRect, COLORREF color);

// Use this to draw text if you need to.
void DrawTextLine(HWND hWnd, HDC hDC, const char* sText, RECT* prText, COLORREF clr);

class SnakeGame {
private:

	directions direction;
	Snake mySnake; 
	unsigned int windowWidth;
	unsigned int windowHeight;

	int scale;
	Point foodPosition;
	bool active;
	std::queue<directions> input;

	// functions
	int randomGenerator() const;
	void drawObstacles(HDC hDC) const;
	void endGame(HWND hWnd);
	RECT getRectangle(HDC hDC, Point leftTop) const;
	void drawSnake(HDC hDC, Node* snake);



public:
	SnakeGame();
	void Initialize(HWND hWnd);
	void OnTimer(HWND hWnd);
	bool OnKeyDown(WPARAM keyCode);

};

