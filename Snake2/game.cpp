
# include <Windows.h>
# include <string>
# include "SnakeGame.h"

SnakeGame snakeGame;

namespace game
{

	// This is called when the application is launched.
	bool Initialize(HWND hWnd)
	{
		snakeGame.Initialize(hWnd);
		return true;

	}
	

	// This is called periodically. Use it to update your game state and draw to the window.
	void CALLBACK OnTimer(HWND hWnd, UINT Msg, UINT_PTR idTimer, DWORD dwTime)
	{
		snakeGame.OnTimer(hWnd);
	}

	// This is called when the user presses a key on the keyboard.
	// Use this to change the direction of your snake.
	bool OnKeyDown(WPARAM keyCode)
	{
		return snakeGame.OnKeyDown(keyCode);
	}
}